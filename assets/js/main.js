var option0 = {
  strings: [ "Tasi ^1000 tējas"],
  typeSpeed: 300
}
var option1 = {
  strings: [ "^500Arbatos ^1000 puodelis"],
  typeSpeed: 300
}
var option2 = {
  strings: [ "^1000 O ceasca ^1000 de ceai"],
  typeSpeed: 300
}
var option3 = {
  strings: [ "^1800Filiżanka ^1000 herbaty"],
  typeSpeed: 300
}
var option4 = {
  strings: [ "^2000 Cupán ^1000 tae"],
  typeSpeed: 300
}
var option5 = {
  strings: [ "A ^2000 cup ^2000 of ^1000 tea"],
  typeSpeed: 800
}

var typed0 = new Typed(".animated_title0", option0);
var typed1 = new Typed(".animated_title1", option1);
var typed2 = new Typed(".animated_title2", option2);
var typed3 = new Typed(".animated_title3", option3);
var typed4 = new Typed(".animated_title4", option4);
// var typed5 = new Typed(".animated_title5", option5);

document.addEventListener("DOMContentLoaded", function() {
  console.log("document loaded");
  $('#about').click(function () {
      $(this).removeAttr("href"),
      $('#reveal').slideToggle();
    }
  );
  $('#info').click(function () {
      $(this).removeAttr("href"),
      $('#reveal-body').slideToggle();
    }
  );
  $('.close-intro').click(function () {
      $(this).removeAttr("href"),
      $('#reveal').slideToggle();
    }
  );
  $('.close-body').click(function () {
      $(this).removeAttr("href"),
      $('#reveal-body').slideToggle();
    }
  );

  var $grid = $('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 400,
    percentPosition: true,
  });
  // layout Masonry after each image loads
  $grid.imagesLoaded().progress( function() {
    $grid.masonry('layout');
  });
});

$(".more-commits").click(function(){
  $(this).removeAttr("href"),
  $("div.more-git-commits").slideToggle()
})
